package com.bdpiparva.backend.service;

import com.bdpiparva.backend.database.InMemoryDB;
import com.bdpiparva.backend.exception.RecordNotFoundException;
import com.bdpiparva.backend.exception.UnprocessableEntityException;
import com.bdpiparva.backend.model.Account;
import org.jvnet.hk2.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;

@Service
public class AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);
    private final InMemoryDB inMemoryDB;
    private Map<String, Object> mutexes = new ConcurrentHashMap<>();

    @Inject
    public AccountService(InMemoryDB inMemoryDB) {
        this.inMemoryDB = inMemoryDB;
    }

    public void create(Account account) {
        inMemoryDB.create(account);
    }

    public void credit(String accountId, BigDecimal amount) {
        bombIfAmountIsNotAcceptable(amount, "credit");

        Account account = inMemoryDB.findById(accountId);
        recordNotFoundIfNull(account, accountId);

        LOGGER.debug("Taking a lock for crediting account: " + accountId);
        synchronized (mutexes.computeIfAbsent(accountId.intern(), id -> new Object())) {
            inMemoryDB.update(account.toBuilder()
                    .balance(account.getBalance().add(amount))
                    .build());
            LOGGER.info(format("Account %s is successfully credited by amount %s.", accountId, amount));
        }
        LOGGER.debug("Releasing a lock for account: " + accountId);
    }

    public Account findById(String accountId) {
        return inMemoryDB.findById(accountId);
    }

    public void transfer(String fromAccountId, String toAccountId, BigDecimal amount) {
        bombIfAmountIsNotAcceptable(amount, "transfer");

        Account fromAccount = inMemoryDB.findById(fromAccountId);
        recordNotFoundIfNull(fromAccount, fromAccountId);

        Account toAccount = inMemoryDB.findById(toAccountId);
        recordNotFoundIfNull(toAccount, toAccountId);

        synchronized (mutexes.computeIfAbsent(fromAccountId.intern(), accountId -> new Object())) {
            if (fromAccount.getBalance().compareTo(amount) < 0) {
                throw new UnprocessableEntityException(format("Account '%s' does not have sufficient credit to make a transaction of amount '%s'.", fromAccountId, amount));
            }

            synchronized (mutexes.computeIfAbsent(toAccountId.intern(), toId -> new Object())) {
                LOGGER.info(format("Deducting %s from account %s", amount, fromAccountId));
                inMemoryDB.update(fromAccount.toBuilder()
                        .balance(fromAccount.getBalance().subtract(amount))
                        .build());

                LOGGER.info(format("Crediting account %s with amount %s", toAccountId, amount));
                inMemoryDB.update(toAccount.toBuilder()
                        .balance(toAccount.getBalance().add(amount))
                        .build());
            }
        }

    }

    private void bombIfAmountIsNotAcceptable(BigDecimal amount, final String action) {
        unprocessableIf(amount.compareTo(BigDecimal.ONE) < 0, format("Amount '%s' is not acceptable for '%s'.", amount.round(MathContext.DECIMAL32), action));
    }

    private void recordNotFoundIfNull(Account object, String withId) {
        if (object == null) {
            throw new RecordNotFoundException(RecordNotFoundException.Entity.Account, withId);
        }
    }

    private void unprocessableIf(boolean condition, String message) {
        if (condition) {
            throw new UnprocessableEntityException(message);
        }
    }
}

## Revolut Backend Test

Design and implement a RESTful API (including data model and the backing implementation) for
money transfers between accounts.

## Table of contents

- [Prerequisite](#prerequisite)
- [Build](#build)
- [CI](#ci)
- [Run](#run)
- [APIs Example](#apis-example)

### Prerequisite
- Java version 8 or above

### Build
Run a gradle task `./gradlew clean asseble` to build the project. This will generate the packages (.zip and .tat) under `build/distributions/`.

### CI

- Execute `./gradlew clean assemble test` to run unit test
- Execute `./gradlew clean assemble integrationTest` to run integration test(here: api tests)
- Execute `./gradlew clean assemble check` to run all tests


### Run

#### Development
Execute command `./gradlew clean assemble run` to run the server in development mode.
 
#### Production
1. [Build](#build) the packages.
2. Unzip the package of your choice(backend-test-1.0.0.tar or backend-test-1.0.0.zip) from `build/distributions/`.
3. Go to extracted directory.
3. Execute `./bin/backend-test` to start the server.

> e.g. Execute chain of command in project dir

```bash
./gradlew clean assemble; cd build/distributions; unzip backend-test-1.0.0.zip; cd backend-test-1.0.0; ./bin/backend-test
```

### Demonstration of API using tests
- See [APITest](src/test-integration/java/com/bdpiparva/backend/APITest.java) for more information.

### APIs Example
- To create an account

```bash
curl http://localhost:8080/api/account/create \
  -X POST \
  -H 'Content-Type: application/json' \
  -d '{ "firstname": "Mr.Data", "mobileNumber": "432423849"}'
```

- To credit an account

```bash
curl http://localhost:8080/api/account/:account_id/credit/:amount \
  -X PATCH \
  -H 'Content-Type: application/json; charset=UTF-8'
```

- To check balance

```bash
curl "http://localhost:8080/api/account/:account_id/balance" \
  -X GET \
  -H 'Content-Type: application/json; charset=UTF-8'
```

- To transfer money

```bash
curl "http://localhost:8080/api/account/:from_account/transfer/:to_account/:amount" \
  -X PATCH \
  -H 'Content-Type: application/json; charset=UTF-8'
```

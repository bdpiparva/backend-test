package com.bdpiparva.backend.exception;

import static java.lang.String.format;

public class RecordNotFoundException extends HttpException {
    public RecordNotFoundException(Entity entity, Object value) {
        super(404, format("%s with %s '%s' doesn't exist.", entity.entityName, entity.identifier, value));
    }

    public enum Entity {
        Account("Account", "id");

        private final String entityName;
        private final String identifier;

        Entity(String entityName, String identifier) {
            this.entityName = entityName;
            this.identifier = identifier;
        }
    }
}

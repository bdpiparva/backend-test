package com.bdpiparva.backend.config;

import com.bdpiparva.backend.dto.MessageDTO;
import com.bdpiparva.backend.exception.ConflictException;
import com.bdpiparva.backend.exception.HttpException;
import com.bdpiparva.backend.exception.RecordNotFoundException;
import com.bdpiparva.backend.exception.UnprocessableEntityException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.ws.rs.core.Response;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class HttpExceptionMapperTest {
    private static Stream<HttpException> allHttpExceptions() {
        return Stream.of(
                new ConflictException("Conflict"),
                new RecordNotFoundException(RecordNotFoundException.Entity.Account, "not found"),
                new UnprocessableEntityException("unprocessable")
        );
    }

    @ParameterizedTest
    @MethodSource("allHttpExceptions")
    void shouldConvertHttpExceptionToResponse(HttpException thrown) {
        HttpExceptionMapper mapper = new HttpExceptionMapper();

        Response response = mapper.toResponse(thrown);

        assertThat(response.getStatus()).isEqualTo(thrown.getStatus());
        assertThat(response.getEntity()).isEqualTo(MessageDTO.from(thrown));
    }
}

package com.bdpiparva.backend.dto;

import com.bdpiparva.backend.model.Account;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Objects;

public class AccountDTO {
    @JsonProperty
    private String id;
    @JsonProperty
    private String firstname;
    @JsonProperty
    private String lastname;
    @JsonProperty
    private String location;
    @JsonProperty("mobile_number")
    private String mobileNumber;
    @JsonProperty
    private BigDecimal balance = BigDecimal.ZERO;

    public String getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getLocation() {
        return location;
    }


    public String getMobileNumber() {
        return mobileNumber;
    }


    public BigDecimal getBalance() {
        return balance == null ? BigDecimal.ZERO : balance;
    }

    public AccountDTO id(String id) {
        this.id = id;
        return this;
    }

    public AccountDTO firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public AccountDTO lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }


    public AccountDTO location(String location) {
        this.location = location;
        return this;
    }

    public AccountDTO mobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
        return this;
    }

    public AccountDTO balance(BigDecimal balance) {
        this.balance = balance == null ? BigDecimal.ZERO : balance;
        return this;
    }

    public static AccountDTO from(Account account) {
        return new AccountDTO()
                .id(account.getId())
                .firstname(account.getFirstname())
                .lastname(account.getLastname())
                .mobileNumber(account.getMobileNumber())
                .location(account.getLocation())
                .balance(account.getBalance());
    }

    public Account toAccount() {
        return toAccountBuilder().build();
    }

    public Account.AccountBuilder toAccountBuilder() {
        return Account.builder()
                .id(id)
                .firstname(firstname)
                .lastname(lastname)
                .location(location)
                .mobileNumber(mobileNumber)
                .balance(balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDTO that = (AccountDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstname, that.firstname) &&
                Objects.equals(lastname, that.lastname) &&
                Objects.equals(location, that.location) &&
                Objects.equals(mobileNumber, that.mobileNumber) &&
                Objects.equals(balance, that.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, location, mobileNumber, balance);
    }
}

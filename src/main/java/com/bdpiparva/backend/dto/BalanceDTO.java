package com.bdpiparva.backend.dto;

import com.bdpiparva.backend.model.Account;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Objects;

public class BalanceDTO {
    @JsonProperty("account_id")
    private String accountId;
    @JsonProperty
    private BigDecimal balance;

    public BalanceDTO() {
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getAccountId() {
        return accountId;
    }

    public static BalanceDTO from(Account account) {
        BalanceDTO dto = new BalanceDTO();
        dto.accountId = account.getId();
        dto.balance = account.getBalance();
        return dto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BalanceDTO that = (BalanceDTO) o;
        return Objects.equals(accountId, that.accountId) &&
                Objects.equals(balance, that.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, balance);
    }
}

package com.bdpiparva.backend;

import com.bdpiparva.backend.dto.AccountDTO;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@ExtendWith(RunWithServer.class)
class APITest {

    @Nested
    class Create {
        @Test
        void shouldGetAccountIdOnCreationOfAccount() {
            AccountDTO payload = new AccountDTO()
                    .firstname("Sheldon")
                    .lastname("Cooper")
                    .location("Pasadena")
                    .mobileNumber("9898989898989");

            AccountDTO account = create(payload);

            assertThat(account.getId()).isNotNull();
            assertThat(account.getFirstname()).isEqualTo(payload.getFirstname());
            assertThat(account.getLastname()).isEqualTo(payload.getLastname());
            assertThat(account.getLocation()).isEqualTo(payload.getLocation());
            assertThat(account.getMobileNumber()).isEqualTo(payload.getMobileNumber());
            assertThat(account.getBalance()).isEqualTo(BigDecimal.ZERO);
        }
    }

    @Nested
    class Balance {
        @Test
        void shouldGetBalanceOfGivenAccountId() {
            AccountDTO account = create(new AccountDTO().firstname("Sheldon").lastname("Cooper"));

            credit(account.getId(), new BigDecimal(555));

            balance(account.getId())
                    .then()
                    .statusCode(200)
                    .body("balance", equalTo(555));
        }

        @Test
        void shouldReturnRecordNotFoundWhenAccountForGivenAccountIdDoesNotExist() {
            balance("non-existent-account-id")
                    .then()
                    .statusCode(404)
                    .body("message", equalTo("Account with id 'non-existent-account-id' doesn't exist."));
        }
    }

    @Nested
    class Credit {
        private AccountDTO account;

        @BeforeEach
        void setUp() {
            account = create(new AccountDTO().firstname("Sheldon").lastname("Cooper"));

            assertThat(account.getId()).isNotNull();
        }

        @Test
        void shouldReturnUnprocessableEntityWhenGivenNegativeAmount() {
            credit(account.getId(), new BigDecimal(-1000))
                    .then()
                    .statusCode(422)
                    .body("message", equalTo("Amount '-1000' is not acceptable for 'credit'."));

        }

        @Test
        void shouldReturnUnprocessableEntityWhenGivenZeroAsAmountToCredit() {
            credit(account.getId(), new BigDecimal(0))
                    .then()
                    .statusCode(422)
                    .body("message", equalTo("Amount '0' is not acceptable for 'credit'."));

        }


        @Test
        void shouldReturnRecordNotFoundWhenGivenUnExistentAccount() {
            credit("non-existent-account-id", new BigDecimal(10))
                    .then()
                    .statusCode(404)
                    .body("message", equalTo("Account with id 'non-existent-account-id' doesn't exist."));

        }

        @Test
        void shouldCreditTheAccountWithGivenAmount() {
            assertThat(account.getBalance()).isEqualTo(new BigDecimal(0));

            credit(account.getId(), new BigDecimal(11.124))
                    .then()
                    .statusCode(200)
                    .body("balance", equalTo(11.124f));

            credit(account.getId(), new BigDecimal(50.285))
                    .then()
                    .statusCode(200)
                    .body("balance", equalTo(61.409f));
        }
    }

    @Nested
    class Transfer {
        private AccountDTO from, to;

        @BeforeEach
        void setUp() {
            from = create(new AccountDTO().firstname("Sheldon").lastname("Cooper"));
            to = create(new AccountDTO().firstname("Leonard").lastname("Hofstadter"));

            assertThat(from.getId()).isNotNull();
            assertThat(to.getId()).isNotNull();

            credit(from.getId(), new BigDecimal(50000)).then().body("balance", equalTo(50000));
            credit(to.getId(), new BigDecimal(25000)).then().body("balance", equalTo(25000));
        }

        @Test
        void shouldTransferMoneyFromOneAccountToAnother() {
            transfer(from.getId(), to.getId(), new BigDecimal(15000))
                    .then()
                    .body("balance", equalTo(35000));

            balance(to.getId())
                    .then()
                    .assertThat()
                    .body("balance", equalTo(40000));
        }

        @Test
        void shouldReturnRecordNotFoundWhenSourceAccountDoesNotExist() {
            transfer("non-existent-source-account-id", to.getId(), new BigDecimal(15000))
                    .then()
                    .statusCode(404)
                    .body("message", equalTo("Account with id 'non-existent-source-account-id' doesn't exist."));
        }

        @Test
        void shouldReturnRecordNotFoundWhenTargetAccountDoesNotExist() {
            transfer(from.getId(), "non-existent-target-account-id", new BigDecimal(15000))
                    .then()
                    .statusCode(404)
                    .body("message", equalTo("Account with id 'non-existent-target-account-id' doesn't exist."));
        }

        @Test
        void shouldReturnConflictWhenGivenSameIdFromSourceAndTargetAccounts() {
            transfer("1", "1", new BigDecimal(15000))
                    .then()
                    .statusCode(409)
                    .body("message", equalTo("Can't make a transfer to the same account."));
        }

        @Test
        void shouldReturnUnprocessableEntityWhenSourceAccountDoesNotHaveEnoughCreditToMakeTransfer() {
            transfer(from.getId(), to.getId(), new BigDecimal(55000))
                    .then()
                    .statusCode(422)
                    .body("message", equalTo(format("Account '%s' does not have sufficient credit to make a transaction of amount '55000'.", from.getId())));
        }
    }

    private static AccountDTO create(AccountDTO accountToCreate) {
        return given()
                .contentType(ContentType.JSON)
                .body(accountToCreate)
                .post("/api/account/create")
                .thenReturn()
                .as(AccountDTO.class);
    }

    private static Response balance(String accountId) {
        return given()
                .contentType(ContentType.JSON)
                .get("/api/account/{account_id}/balance", accountId);
    }

    private static Response credit(String accountId, BigDecimal amount) {
        return given()
                .contentType(ContentType.JSON)
                .patch("/api/account/{account_id}/credit/{amount}", accountId, amount);
    }

    private static Response transfer(String from, String to, BigDecimal amount) {
        return given()
                .contentType(ContentType.JSON)
                .patch("/api/account/{from}/transfer/{to}/{amount}", from, to, amount);
    }
}

package com.bdpiparva.backend.controller;

import com.bdpiparva.backend.dto.AccountDTO;
import com.bdpiparva.backend.dto.BalanceDTO;
import com.bdpiparva.backend.exception.ConflictException;
import com.bdpiparva.backend.exception.RecordNotFoundException;
import com.bdpiparva.backend.model.Account;
import com.bdpiparva.backend.service.AccountIdGenerationService;
import com.bdpiparva.backend.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

import static com.bdpiparva.backend.exception.RecordNotFoundException.Entity;

@Path("/account")
public class AccountController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);
    private final AccountService accountService;
    private final AccountIdGenerationService accountIdGenerationService;

    @Inject
    public AccountController(AccountService accountService, AccountIdGenerationService accountIdGenerationService) {
        this.accountService = accountService;
        this.accountIdGenerationService = accountIdGenerationService;
    }

    @GET
    @Path("/{account_id}/balance")
    @Produces(MediaType.APPLICATION_JSON)
    public BalanceDTO checkBalance(@PathParam("account_id") String accountId) {
        Account account = accountService.findById(accountId);

        if (account == null) {
            throw new RecordNotFoundException(Entity.Account, accountId);
        }

        return BalanceDTO.from(account);
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public AccountDTO createAccount(AccountDTO accountDTO) {
        Account accountToCreate = accountDTO
                .toAccountBuilder()
                .id(accountIdGenerationService.generate())
                .build();

        accountService.create(accountToCreate);

        return AccountDTO.from(accountToCreate);
    }

    @PATCH
    @Path("/{account_id}/credit/{amount}")
    @Produces(MediaType.APPLICATION_JSON)
    public BalanceDTO credit(@PathParam("account_id") String accountId,
                             @PathParam("amount") BigDecimal amount) {
        accountService.credit(accountId, amount);
        return BalanceDTO.from(accountService.findById(accountId));
    }

    @PATCH
    @Path("/{from_account}/transfer/{to_account}/{amount}")
    @Produces(MediaType.APPLICATION_JSON)
    public BalanceDTO transfer(@PathParam("from_account") String fromAccountId,
                               @PathParam("to_account") String toAccountId,
                               @PathParam("amount") BigDecimal amount) {

        if (fromAccountId.equalsIgnoreCase(toAccountId)) {
            LOGGER.error("Can't make a transfer to the same account.");
            throw new ConflictException("Can't make a transfer to the same account.");
        }

        accountService.transfer(fromAccountId, toAccountId, amount);
        return BalanceDTO.from(accountService.findById(fromAccountId));
    }
}

package com.bdpiparva.backend.config;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * This is required to scan the resource classes e.g. controller, service, etc.
 */
public class ResourceConfiguration extends ResourceConfig {
    public ResourceConfiguration() {
        packages(packagesToScan())
                .register(HttpExceptionMapper.class)
                .register(new Binder());
    }

    static String[] packagesToScan() {
        return new String[]{
                "com.bdpiparva.backend.model",
                "com.bdpiparva.backend.service",
                "com.bdpiparva.backend.database",
                "com.bdpiparva.backend.controller"
        };
    }
}

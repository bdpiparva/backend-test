package com.bdpiparva.backend.database;

import com.bdpiparva.backend.model.Account;
import org.jvnet.hk2.annotations.Service;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;

@Service
public class InMemoryDB {
    private static final Map<String, Account> ACCOUNTS = new ConcurrentHashMap<>();

    public void create(Account account) {
        ACCOUNTS.put(account.getId(), account);
    }

    public Collection<Account> all() {
        return ACCOUNTS.values();
    }

    public Account findById(String id) {
        return ACCOUNTS.get(id);
    }

    public void update(Account account) {
        if (!ACCOUNTS.containsKey(account.getId())) {
            throw new RuntimeException(format("Account with id '%s' doesn't exist.", account.getId()));
        }

        ACCOUNTS.put(account.getId(), account);
    }
}

package com.bdpiparva.backend.controller;

import com.bdpiparva.backend.dto.AccountDTO;
import com.bdpiparva.backend.dto.BalanceDTO;
import com.bdpiparva.backend.exception.ConflictException;
import com.bdpiparva.backend.exception.RecordNotFoundException;
import com.bdpiparva.backend.model.Account;
import com.bdpiparva.backend.service.AccountIdGenerationService;
import com.bdpiparva.backend.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class AccountControllerTest {
    @Mock
    private AccountService accountService;
    @Mock
    private AccountIdGenerationService accountIdGenerationService;

    @InjectMocks
    private AccountController accountController;
    private ArgumentCaptor<Account> accountArgumentCaptor;

    @BeforeEach
    void setUp() {
        initMocks(this);
        accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
    }

    @Nested
    class Create {

        @Test
        void shouldCallAccountServiceToCreateAnAccount() {
            AccountDTO payload = new AccountDTO()
                    .firstname("Sheldon")
                    .lastname("Cooper")
                    .location("0,0,0")
                    .mobileNumber("9999999999");
            when(accountIdGenerationService.generate()).thenReturn("1");

            AccountDTO accountDTO = accountController.createAccount(payload);

            InOrder inOrder = inOrder(accountIdGenerationService, accountService);
            inOrder.verify(accountIdGenerationService).generate();
            inOrder.verify(accountService).create(accountArgumentCaptor.capture());

            assertThat(accountArgumentCaptor.getValue()).isEqualTo(accountDTO.toAccount());
            assertThat(accountDTO.getId()).isEqualTo("1");
        }
    }

    @Nested
    class Balance {
        @Test
        void shouldReturnBalanceOfAnAccount() {
            Account account = Account.builder().id("1").balance(new BigDecimal(200)).build();
            when(accountService.findById(account.getId())).thenReturn(account);

            BalanceDTO balanceDTO = accountController.checkBalance("1");

            assertThat(balanceDTO.getBalance()).isEqualTo(new BigDecimal(200));
        }

        @Test
        void shouldBombWhenAccountWithIdDoesNotExist() {
            when(accountService.findById("1")).thenReturn(null);

            assertThatCode(() -> accountController.checkBalance("1"))
                    .isInstanceOf(RecordNotFoundException.class)
                    .hasMessage("Account with id '1' doesn't exist.");
        }
    }

    @Nested
    class Credit {
        @Test
        void shouldCallAccountServiceToCreditAnAccount() {
            String accountId = "1";
            when(accountService.findById(accountId)).thenReturn(mock(Account.class));

            accountController.credit(accountId, new BigDecimal(15.30));

            verify(accountService).credit(accountId, new BigDecimal(15.30));
        }
    }

    @Nested
    class Transfer {
        @Test
        void shouldBombIfBothAccountIdIsSame() {
            assertThatCode(() -> accountController.transfer("1", "1", new BigDecimal(10)))
                    .isInstanceOf(ConflictException.class)
                    .hasMessage("Can't make a transfer to the same account.");
        }

        @Test
        void shouldCallAccountServiceToMakeTransfer() {
            Account account = Account.builder().id("1").build();
            when(accountService.findById(account.getId())).thenReturn(account);

            BalanceDTO dto = accountController.transfer("1", "2", new BigDecimal(10));

            assertThat(dto).isEqualTo(BalanceDTO.from(account));
            verify(accountService).transfer("1", "2", new BigDecimal(10));
        }
    }
}

package com.bdpiparva.backend.service;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AccountIdGenerationServiceTest {
    @Test
    void shouldGenerateUniqueIdWhenCalled() {
        AccountIdGenerationService service = new AccountIdGenerationService();

        assertThat(service.generate()).isNotSameAs(service.generate());
    }
}

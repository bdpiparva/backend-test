package com.bdpiparva.backend.database;

import com.bdpiparva.backend.model.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class InMemoryDBTest {
    private InMemoryDB inMemoryDB;

    @BeforeEach
    void setUp() {
        inMemoryDB = new InMemoryDB();
    }

    @Test
    void shouldReturnEmptyListWhenNoAccountIsCreated() {
        assertThat(inMemoryDB.all()).isEmpty();
    }

    @Test
    void shouldCreateAnAccount() {
        Account account = mock(Account.class);
        when(account.getId()).thenReturn("1");

        inMemoryDB.create(account);

        assertThat(inMemoryDB.all())
                .hasSize(1)
                .contains(account);
    }

    @Nested
    class FindAccount {
        @Test
        void shouldReturnNullWhenAccountWithIdDoesNotExist() {
            assertThat(inMemoryDB.findById("non-existing-id")).isNull();
        }

        @Test
        void shouldFindAccountById() {
            Account account = mock(Account.class);
            when(account.getId()).thenReturn("1");
            inMemoryDB.create(account);

            Account accountFromDB = inMemoryDB.findById(account.getId());

            assertThat(accountFromDB).isEqualTo(account);
        }
    }

    @Nested
    class Update {
        @Test
        void shouldBombWhenAccountWithGivenIdDoesNotExist() {
            assertThatCode(() -> inMemoryDB.update(Account.builder().id("non-existing-id").build()))
                    .isInstanceOf(RuntimeException.class)
                    .hasMessage("Account with id 'non-existing-id' doesn't exist.");
        }

        @Test
        void shouldCreditAccountByGivenAmount() {
            Account account = Account.builder().id("1").balance(new BigDecimal(10.00)).build();
            inMemoryDB.create(account);

            assertThat(account.getBalance()).isEqualTo(new BigDecimal(10));
            inMemoryDB.update(Account.builder().id("1").balance(new BigDecimal(20.50)).build());

            assertThat(inMemoryDB.findById("1").getBalance()).isEqualTo(new BigDecimal(20.50));
        }
    }
}

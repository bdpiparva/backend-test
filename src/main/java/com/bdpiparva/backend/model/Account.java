package com.bdpiparva.backend.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Account {
    private String id;
    private String firstname;
    private String lastname;
    private String location;
    private String mobileNumber;
    private BigDecimal balance;

    public BigDecimal getBalance() {
        return this.balance == null ? BigDecimal.ZERO : this.balance;
    }

    public String getId() {
        return this.id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public String getLocation() {
        return this.location;
    }

    public String getMobileNumber() {
        return this.mobileNumber;
    }

    Account(String id, String firstname, String lastname, String location, String mobileNumber, BigDecimal balance) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.location = location;
        this.mobileNumber = mobileNumber;
        this.balance = balance;
    }

    public static Account.AccountBuilder builder() {
        return new Account.AccountBuilder();
    }

    public Account.AccountBuilder toBuilder() {
        return (new Account.AccountBuilder()).id(this.id).firstname(this.firstname).lastname(this.lastname).location(this.location).mobileNumber(this.mobileNumber).balance(this.balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(firstname, account.firstname) &&
                Objects.equals(lastname, account.lastname) &&
                Objects.equals(location, account.location) &&
                Objects.equals(mobileNumber, account.mobileNumber) &&
                Objects.equals(balance, account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, location, mobileNumber, balance);
    }

    public static class AccountBuilder {
        private String id;
        private String firstname;
        private String lastname;
        private String location;
        private String mobileNumber;
        private BigDecimal balance;

        AccountBuilder() {
        }

        public Account.AccountBuilder id(String id) {
            this.id = id;
            return this;
        }

        public Account.AccountBuilder firstname(String firstname) {
            this.firstname = firstname;
            return this;
        }

        public Account.AccountBuilder lastname(String lastname) {
            this.lastname = lastname;
            return this;
        }

        public Account.AccountBuilder location(String location) {
            this.location = location;
            return this;
        }

        public Account.AccountBuilder mobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
            return this;
        }

        public Account.AccountBuilder balance(BigDecimal balance) {
            this.balance = balance;
            return this;
        }

        public Account build() {
            return new Account(this.id, this.firstname, this.lastname, this.location, this.mobileNumber, this.balance);
        }

        @Override
        public String toString() {
            return "AccountBuilder{" +
                    "id='" + id + '\'' +
                    ", firstname='" + firstname + '\'' +
                    ", lastname='" + lastname + '\'' +
                    ", location='" + location + '\'' +
                    ", mobileNumber='" + mobileNumber + '\'' +
                    ", balance=" + balance +
                    '}';
        }
    }
}

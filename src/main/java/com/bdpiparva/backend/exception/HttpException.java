package com.bdpiparva.backend.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpException extends RuntimeException {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private int status;

    HttpException(int status, String message) {
        super(message);
        this.status = status;
        logger.error(message);
    }

    public int getStatus() {
        return status;
    }
}

package com.bdpiparva.backend.server;

import com.bdpiparva.backend.config.ResourceConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.component.LifeCycle;
import org.glassfish.jersey.servlet.ServletContainer;

import java.util.ArrayList;
import java.util.List;

import static org.eclipse.jetty.servlet.ServletContextHandler.NO_SESSIONS;

/**
 * ApiServer is responsible to start the server.
 * For simplicity, all configuration has been defined in this class itself.
 * Ideally, configurations should be read from a dedicated configuration or properties file.
 */
public class ApiServer {
    private static final String API_PATH = "/api/*";
    private static final String CONTEXT_PATH = "/";
    private static final int PORT = 8080;
    private final List<LifeCycle.Listener> lifeCycleListener = new ArrayList<>();
    private final Server server = new Server(PORT);

    public void addLifeCycleListener(LifeCycle.Listener listener) {
        lifeCycleListener.add(listener);
    }

    public ApiServer start() {
        ServletContextHandler context = new ServletContextHandler(NO_SESSIONS);
        context.setContextPath(CONTEXT_PATH);
        server.setHandler(context);
        lifeCycleListener.forEach(server::addLifeCycleListener);

        ServletHolder servlet = new ServletHolder(new ServletContainer(new ResourceConfiguration()));
        servlet.setInitOrder(0);
        context.addServlet(servlet, API_PATH);
        try {
            server.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    public Server getServer() {
        return server;
    }

    public ApiServer join() throws InterruptedException {
        server.join();
        return this;
    }
}

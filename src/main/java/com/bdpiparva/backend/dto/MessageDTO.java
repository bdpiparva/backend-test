package com.bdpiparva.backend.dto;

import com.bdpiparva.backend.exception.HttpException;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class MessageDTO {
    @JsonProperty
    private String message;

    public MessageDTO() {
    }

    public static MessageDTO from(HttpException e) {
        MessageDTO dto = new MessageDTO();
        dto.message = e.getMessage();
        return dto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageDTO that = (MessageDTO) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }
}

package com.bdpiparva.backend.config;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ResourceConfigurationTest {
    private ResourceConfiguration resourceConfiguration;

    @BeforeEach
    void setUp() {
        resourceConfiguration = new ResourceConfiguration();

    }

    @Test
    void shouldRegisterHttpExceptionMapper() {
        assertThat(resourceConfiguration.isRegistered(HttpExceptionMapper.class)).isTrue();
    }

    @Test
    void shouldRegisterBinder() {
        assertThat(resourceConfiguration.isRegistered(Binder.class)).isTrue();
    }

    @Test
    void shouldScanAnnotatedClassesInPackages() {

        assertThat(ResourceConfiguration.packagesToScan()).contains(
                "com.bdpiparva.backend.model",
                "com.bdpiparva.backend.service",
                "com.bdpiparva.backend.database",
                "com.bdpiparva.backend.controller"
        );
    }
}

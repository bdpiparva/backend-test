package com.bdpiparva.backend;

import com.bdpiparva.backend.server.ApiServer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.component.LifeCycle;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.Extension;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class RunWithServer implements Extension, BeforeEachCallback, AfterEachCallback {
    private Logger logger = LoggerFactory.getLogger(RunWithServer.class);
    private final AtomicReference<ApiServer> ATOMIC_REFERENCE = new AtomicReference<>();
    private final AtomicReference<Event> serverStatus = new AtomicReference<>();
    private final AtomicInteger numberOfRetry = new AtomicInteger(15);

    @Override
    public void beforeEach(ExtensionContext context) throws InterruptedException, TimeoutException, IOException {
        if (ATOMIC_REFERENCE.get() != null) {
            throw new RuntimeException("Application already running");
        }

        ApiServer application = new ApiServer();
        application.addLifeCycleListener(listener);

        Thread applicationThread = new Thread(application::start);
        applicationThread.setDaemon(true);

        ATOMIC_REFERENCE.set(application);
        logger.info("Starting server.");
        applicationThread.start();

        while (serverStatus.get() != Event.Started) {
            int numberOfAttemptLeft = numberOfRetry.getAndDecrement();
            if (numberOfAttemptLeft == 0) {
                throw new TimeoutException("Failed to start server.");
            }
            logger.info(String.format("Waiting for server to start. Number of retry left: %d", numberOfAttemptLeft));
            Thread.sleep(200);
        }
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        Server server = ATOMIC_REFERENCE.get().getServer();
        if (server != null && server.isRunning()) {
            logger.info("Stopping the server.");
            server.stop();
            logger.info("Server stopped successfully.");
            server.destroy();
            ATOMIC_REFERENCE.set(null);
        }
    }

    private LifeCycle.Listener listener = new LifeCycle.Listener() {
        @Override
        public void lifeCycleStarting(LifeCycle event) {
            serverStatus.set(Event.Starting);
        }

        @Override
        public void lifeCycleStarted(LifeCycle event) {
            serverStatus.set(Event.Started);
        }

        @Override
        public void lifeCycleFailure(LifeCycle event, Throwable cause) {
            serverStatus.set(Event.Failure);
        }

        @Override
        public void lifeCycleStopping(LifeCycle event) {
            serverStatus.set(Event.Stopping);
        }

        @Override
        public void lifeCycleStopped(LifeCycle event) {
            serverStatus.set(Event.Stopped);
        }
    };

    enum Event {
        Starting, Started, Failure, Stopping, Stopped;
    }
}

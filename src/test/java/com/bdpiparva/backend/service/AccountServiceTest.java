package com.bdpiparva.backend.service;

import com.bdpiparva.backend.database.InMemoryDB;
import com.bdpiparva.backend.exception.RecordNotFoundException;
import com.bdpiparva.backend.exception.UnprocessableEntityException;
import com.bdpiparva.backend.model.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class AccountServiceTest {
    @Mock
    private InMemoryDB inMemoryDB;

    @InjectMocks
    private AccountService accountService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void shouldCreateAnAccountFromCreateAccountInfo() {
        Account account = Account.builder().id("1").build();

        accountService.create(account);

        verify(inMemoryDB).create(account);
    }

    @Test
    void shouldCallInMemoryDBToFindAccountById() {
        accountService.findById("1");

        verify(inMemoryDB).findById("1");
    }

    @Nested
    class Credit {
        @Test
        void shouldBombIfGivenAmountIsZero() {
            assertThatCode(() -> accountService.credit("1", new BigDecimal(0.0)))
                    .isInstanceOf(UnprocessableEntityException.class)
                    .hasMessage("Amount '0' is not acceptable for 'credit'.");
        }

        @Test
        void shouldBombIfGivenAmountIsNegative() {
            assertThatCode(() -> accountService.credit("1", new BigDecimal(-10.1)))
                    .isInstanceOf(UnprocessableEntityException.class)
                    .hasMessage("Amount '-10.10000' is not acceptable for 'credit'.");
        }

        @Test
        void shouldCreditAmountByGivenAmount() {
            Account account = Account.builder().id("1").build();
            when(inMemoryDB.findById(account.getId())).thenReturn(account);

            accountService.credit(account.getId(), new BigDecimal((10.50)));

            ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
            verify(inMemoryDB).update(accountArgumentCaptor.capture());

            assertThat(accountArgumentCaptor.getValue().getBalance()).isEqualTo(new BigDecimal(10.50));
        }
    }

    @Nested
    class Transfer {
        @Test
        void shouldBombIfGivenAmountIsZero() {
            assertThatCode(() -> accountService.transfer(null, null, new BigDecimal(0)))
                    .isInstanceOf(UnprocessableEntityException.class)
                    .hasMessage("Amount '0' is not acceptable for 'transfer'.");
        }

        @Test
        void shouldBombIfGivenAmountIsNegative() {
            assertThatCode(() -> accountService.transfer(null, null, new BigDecimal(-10.00)))
                    .isInstanceOf(UnprocessableEntityException.class)
                    .hasMessage("Amount '-10' is not acceptable for 'transfer'.");
        }

        @Test
        void shouldBombIfAccountFromWhichTransferIsGoingToInitiateDoesNotExist() {
            assertThatCode(() -> accountService.transfer("non-existing-account", null, new BigDecimal(10)))
                    .isInstanceOf(RecordNotFoundException.class)
                    .hasMessage("Account with id 'non-existing-account' doesn't exist.");
        }

        @Test
        void shouldBombIfAccountToWhichTransferIsGoingToHappenDoesNotExist() {
            Account account = Account.builder().id("1").build();
            when(inMemoryDB.findById(account.getId())).thenReturn(account);

            assertThatCode(() -> accountService.transfer("1", "non-existing-account", new BigDecimal(10)))
                    .isInstanceOf(RecordNotFoundException.class)
                    .hasMessage("Account with id 'non-existing-account' doesn't exist.");
        }

        @Test
        void shouldBombIfFromAccountDoesNotHaveEnoughBalanceToMakeTransfer() {
            Account fromAccount = Account.builder().id("1").balance(new BigDecimal(5.0)).build();
            Account toAccount = Account.builder().id("2").build();
            when(inMemoryDB.findById(fromAccount.getId())).thenReturn(fromAccount);
            when(inMemoryDB.findById(toAccount.getId())).thenReturn(toAccount);

            assertThatCode(() -> accountService.transfer("1", "2", new BigDecimal(10)))
                    .isInstanceOf(UnprocessableEntityException.class)
                    .hasMessage("Account '1' does not have sufficient credit to make a transaction of amount '10'.");

        }

        @Test
        void shouldMakeTransfer() {
            Account fromAccount = Account.builder().id("1").balance(new BigDecimal(15.0)).build();
            Account toAccount = Account.builder().id("2").balance(new BigDecimal(0.0)).build();
            when(inMemoryDB.findById(fromAccount.getId())).thenReturn(fromAccount);
            when(inMemoryDB.findById(toAccount.getId())).thenReturn(toAccount);

            accountService.transfer("1", "2", new BigDecimal(10));

            InOrder inOrder = inOrder(inMemoryDB);
            inOrder.verify(inMemoryDB).update(Account.builder().id("1").balance(new BigDecimal(5.0)).build());
            inOrder.verify(inMemoryDB).update(Account.builder().id("2").balance(new BigDecimal(10.0)).build());
        }
    }
}

package com.bdpiparva.backend.service;

import javax.inject.Singleton;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This is just an example id generation service.
 */
@Singleton
public class AccountIdGenerationService {
    private static final AtomicLong INDEX = new AtomicLong();

    public String generate() {
        return String.format("%s%d", UUID.randomUUID().toString().replaceAll("-", ""), INDEX.getAndIncrement());
    }
}

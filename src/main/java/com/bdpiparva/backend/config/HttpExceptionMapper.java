package com.bdpiparva.backend.config;

import com.bdpiparva.backend.dto.MessageDTO;
import com.bdpiparva.backend.exception.HttpException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * This class will convert custom http exceptions to response object
 */
public class HttpExceptionMapper implements ExceptionMapper<HttpException> {
    @Override
    public Response toResponse(HttpException exception) {
        return Response.status(exception.getStatus()).entity(MessageDTO.from(exception)).build();
    }
}

package com.bdpiparva.backend.config;

import com.bdpiparva.backend.database.InMemoryDB;
import com.bdpiparva.backend.service.AccountIdGenerationService;
import com.bdpiparva.backend.service.AccountService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

class Binder extends AbstractBinder {
    @Override
    protected void configure() {
        bindAsContract(InMemoryDB.class);
        bindAsContract(AccountService.class);
        bindAsContract(AccountIdGenerationService.class);
    }
}

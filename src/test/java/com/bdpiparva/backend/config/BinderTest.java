package com.bdpiparva.backend.config;

import com.bdpiparva.backend.database.InMemoryDB;
import com.bdpiparva.backend.service.AccountService;
import org.glassfish.hk2.api.DynamicConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class BinderTest {

    @Test
    void shouldRegisterContracts() {
        Binder binder = Mockito.spy(new Binder());

        binder.bind(mock(DynamicConfiguration.class));

        verify(binder).configure();
        verify(binder).bindAsContract(AccountService.class);
        verify(binder).bindAsContract(InMemoryDB.class);
        verify(binder).bindAsContract(AccountService.class);
    }
}

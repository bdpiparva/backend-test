package com.bdpiparva.backend.exception;

public class UnprocessableEntityException extends HttpException {
    public UnprocessableEntityException(String message) {
        super(422, message);
    }
}
